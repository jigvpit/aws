#!/bin/bash
# Prepare script to be able to parameterize the environments

#$1 Environment (DEV, UAT, PRD)
#$2 Terraform command (workspace, init, validate, plan, apply, destroy)

echo "Executing command: " $2 "in environment: " $1  

# Prepare script to be able to parameterize the environments
if [ $# -eq 0 ]
then
	echo
	echo "Please inform an environment as an argument."
	echo $1" <dev | uat | prod>"
	echo 
    echo "Please inform or terraform command to be executed."
	echo $2" <init | plan | apply | destroy>"
	exit 1
fi

#### PASS PARAMETER TO TREAT TERRAFORM VERSION ???
TERRAFORM=$TERRAFORM12/terraform

export AWS_ACCOUNT_ID=my_personal_aws_account

echo 'AWS Account ID: ' ${AWS_ACCOUNT_ID}

$TERRAFORM12/terraform --version
echo "start Executing command: " $2 "in environment: " $1  

if [ "$2" == "init" ]; then
    echo "Initializing ...."
    #$TERRAFORM $@  -var account=${AWS_ACCOUNT_ID} || exit 1

    #$TERRAFORM $2  -var-file="../environments/$1.tfvars" || exit 1
    echo $TERRAFORM $2 -reconfigure -backend-config="environments/$1.tfbackend" -var-file="environments/$1.tfvars"
    $TERRAFORM $2 -reconfigure -backend-config="environments/$1.tfbackend" -var-file="environments/$1.tfvars" || exit 1
else
    #echo "else Executing command: " $2 "in environment: " $1  
    # $TERRAFORM validate  -var account=${AWS_ACCOUNT_ID} || exit 1
    # $TERRAFORM $@ -var account=${AWS_ACCOUNT_ID} || exit 2

    #$TERRAFORM validate  -var-file="../environments/$1.tfvars" || exit 1
    echo $TERRAFORM $2 -reconfigure -backend-config="environments/$1.tfbackend" -var-file="environments/$1.tfvars" $3
    $TERRAFORM validate || exit 1
    #$TERRAFORM $2 -reconfigure -backend-config="environments/$1.tfbackend" -var-file="environments/$1.tfvars" $3 || exit 2
    $TERRAFORM $2 -var-file="environments/$1.tfvars" $3 || exit 2
fi