# AWS Terraform Template

This Stack is responsible to build the TEMPLATE stack in AWS.

## Architecture

- Diagram= Confluence/Wiki/Spaces/Dev/pages

## Resources

- VPC
- Subnets
- SG
- S3
- VPN

## Directory structure

- environments -> parameterization of resources.
  - source -> terraform code for resource creation.

## Configurations

- Environments DEV, UAT, PROD
  - Region: us-east-2
  - Terraform version > 0.15.4
  - Details in:
    - environments/dev.tfvars
    - environments/dev.tfbackend

    - environments/uat.tfvars
    - environments/uat.tfbackend

    - environments/prod.tfvars
    - environments/prod.tfbackend

## Versioning '.tfstate'

- Create S3 bucket: <Account-Number>-tfstate
  - example: xxxxxxx-tfstate
  
  - Details in:

    - environments/dev.tfbackend

    - environments/uat.tfbackend

    - environments/prod.tfbackend

## Variables in the directory

- This directory contains the parameters for each environment so we can leave the code flexible to apply in different environments.

  - <https://www.terraform.io/docs/backends/config.html>
  - <https://www.terraform.io/docs/configuration/variables.html>

### Environments

- dev.tfbackend -> backend parameterization.
- dev.tfvars -> features parameterization.

- uat.tfbackend -> backend parameterization.
- uat.tfvars -> features parameterization.

- prod.tfbackend -> backend parameterization.
- prod.tfvars -> features parameterization.

## Configured AWS Credentials

Configure your local stack first in your personal account and test before to move to the Project AWS account.

## Steps for run the project

  -THE SCRIPT OBJECTIVE !!!!!

### Example for Dev Environment

- Install Terraform 0.15.4 from here https://releases.hashicorp.com/terraform/0.15.4/ accordignly to Your operating system 

- For Ubuntu or Ubuntu WSL you can install Terraform 0.15.4 and configure AWS cli credentials with the below command=

```bash
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add - && sudo apt-add-repository "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main" && sudo apt install terraform=0.15.4 awscli -y && aws configure

```


- Clone this Repo following the next steps

```bash
cd Documents && mkdir n1 && cd n1 && git clone https://jigvpit@bitbucket.org/jigvpit/aws.git && cd aws && cd stack && pwd
```

- Shell permissions.

```bash
sudo chmod +x terraform.sh
```

- Workspace management, Isolate states, so if you run "terraform plan" Terraform will not see any existing state for this configuration.

```bash
sudo terraform workspace new n1_dev
```

- Initialize, Prepare your working directory for other commands

```bash
sudo ./terraform.sh dev init
```

- Plan, Show changes required by the current configuration

```bash
sudo ./terraform.sh dev plan
```

- Apply, Create or update infrastructure

```bash
sudo ./terraform.sh dev apply
```

- Destroy, previously-created infrastructure

```bash
sudo ./terraform.sh dev destroy
```